/*
Ici on va empêcher les actions par défaut au sein des appareil mobiles lors du click puis faire jouer le son désiré dans le foyer souhaité.
*/


function touchstartFoyerTricuspidien(e){
  e.preventDefault();
  var stetho = e.target;
  var touch = e.touches[0];//Pour les ipad?
  console.log("touchstart");
  $('foyerTricuspidien').play();
  $('foyerTricuspidien').loop=true;
  $('vesicular').play();
  $('vesicular').loop=true;
  //Là on va écouter le touchend
  stetho.addEventListener('touchend',function(){
    console.log("Touchend");
    $('foyerTricuspidien').pause(); try {$('foyerTricuspidien').currentTime=0; } catch(e) {} $('foyerTricuspidien').loop=false; $('vesicular').pause(); try {$('vesicular').currentTime=0; } catch(e) {} $('vesicular').loop=false;
  });
}

function touchstartFoyerPulmonaire(e){
  e.preventDefault();
  var stetho = e.target;
  var touch = e.touches[0];//Pour les ipad?
  console.log("touchstart");
  $('foyerPulmonaire').play();
  $('foyerPulmonaire').loop=true;
  $('vesicular').play();
  $('vesicular').loop=true;
  //Là on va écouter le touchend
  stetho.addEventListener('touchend',function(){
    console.log("Touchend");
    $('foyerPulmonaire').pause(); try {$('foyerPulmonaire').currentTime=0; } catch(e) {} $('foyerPulmonaire').loop=false; $('vesicular').pause(); try {$('vesicular').currentTime=0; } catch(e) {} $('vesicular').loop=false;
  });
}

function touchstartFoyerMitral(e){
  e.preventDefault();
  var stetho = e.target;
  var touch = e.touches[0];//Pour les ipad?
  console.log("touchstart");
  $('foyerMitral').play();
  $('foyerMitral').loop=true;
  $('vesicular').play();
  $('vesicular').loop=true;
  //Là on va écouter le touchend
  stetho.addEventListener('touchend',function(){
    console.log("Touchend");
    $('foyerMitral').pause(); try {$('foyerMitral').currentTime=0; } catch(e) {} $('foyerMitral').loop=false; $('vesicular').pause(); try {$('vesicular').currentTime=0; } catch(e) {} $('vesicular').loop=false;
  });
}

function touchstartFoyerAortique(e){
  e.preventDefault();
  var stetho = e.target;
  var touch = e.touches[0];//Pour les ipad?
  console.log("touchstart");
  $('foyerAortique').play();
  $('foyerAortique').loop=true;
  $('vesicular').play();
  $('vesicular').loop=true;
  //Là on va écouter le touchend
  stetho.addEventListener('touchend',function(){
    console.log("Touchend");
    $('foyerAortique').pause(); try {$('foyerAortique').currentTime=0; } catch(e) {} $('foyerAortique').loop=false; $('vesicular').pause(); try {$('vesicular').currentTime=0; } catch(e) {} $('vesicular').loop=false;
  });
}

function touchstartPoumons(e){
  e.preventDefault();
  var stetho = e.target;
  var touch = e.touches[0];//Pour les ipad?
  console.log("touchstart")
  $('vesicular').play();
  $('vesicular').loop=true;
  stetho.addEventListener('touchend',function(){
    console.log("Touchend");
    $('vesicular').pause(); try {$('vesicular').currentTime=0; } catch(e) {} $('vesicular').loop=false;
  });
}

document.getElementById("Foyer_pulmonaire").addEventListener('touchstart',touchstartFoyerPulmonaire, false);
/*
Par exemple ici on a pris l'élément dans le .svg qui a l'Id Foyer_pulmonaire, on écoute les événements et s'il y a un 'touchstart' on lance la fonction touchstart
*/
document.getElementById("Foyer_Tricuspidien").addEventListener('touchstart',touchstartFoyerTricuspidien, false);
document.getElementById("Foyer_Mitral").addEventListener('touchstart',touchstartFoyerMitral, false);
document.getElementById("Foyer_Aortique").addEventListener('touchstart',touchstartFoyerAortique, false);
document.getElementById("Poumon_Gauche").addEventListener('touchstart',touchstartPoumons, false);
document.getElementById("Poumon_Sup__rieur_Droit").addEventListener('touchstart',touchstartPoumons, false);
