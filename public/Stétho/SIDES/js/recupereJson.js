// Code récupéré sur le cours publié sur la plateforme OpenClassRoom avec le professeur Baptiste Pesquet sous licence cc by-nc-sa 2.0.

// Sera probablement utile si l'utilisation de fichier .json s'avère souhaitée.

ajaxGet("data/thorax.json", function (reponse) {
// Transforme la réponse en tableau d'objets JavaScript
var sons = JSON.parse(reponse);
// Affiche le titre de chaque film
sons.forEach(function (foyer) {
    console.log(foyer.Foyer);
})
});
